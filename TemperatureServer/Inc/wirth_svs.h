#ifndef __WIRTH_SVS_H
#define __WIRTH_SVS_H


#include <stdbool.h>
#include <stdint.h>

#include "cmsis_os.h"
#include "nrf24.h"


#define WIRTH_SVS_MAX_DEVICES       ((uint8_t)6)
#define WIRTH_SVS_MAX_QUEUE_ENTRIES ((uint8_t)16)

// Wireless thermometer exchange packet. Used for exhange with thermometers.
typedef struct
{
    uint8_t     Version;
    
    int8_t      ChipTemperature;
    
    uint16_t    ChipVoltage;
    
    float       MeterTemperature;
    
    uint32_t    UpTime;
}
WIRTH_SVS_PacketTypeDef;

// Wireless thermometer params and data. Stores info and data of each thermometer.
typedef struct
{
    uint32_t                    LastExchange;
    
    uint32_t                    UpTime;
    
    uint8_t                     AddressLSByte;
    
    uint8_t                     ChipVoltage;
    
    uint8_t                     ChipTemperature;
    
    uint8_t                     PowerSetup;
}
WIRTH_SVS_DeviceTypeDef;

// Wireless thermometer data. Used for storing temperature.
typedef struct
{
    uint32_t                    Time;
    
    float                       Temperature;
    
    WIRTH_SVS_DeviceTypeDef     *Device;
}
WIRTH_SVS_DataTypeDef;

// Handle of wireless thermometers controller.
typedef struct
{
    NRF24_HandleTypeDef         NrfHandle;
    
    WIRTH_SVS_PacketTypeDef     Packet;
    
    WIRTH_SVS_DeviceTypeDef     Devices[WIRTH_SVS_MAX_DEVICES];
    
    osMessageQId                Queue;
    
    uint8_t                     CommonAddressPart[NRF24_MAX_ADDR_LENGTH - 1];    
}
WIRTH_SVS_HandleTypeDef;


void WIRTH_SVS_StartTask(void const * argument);


#endif /* __WIRTH_SVS_H */
