#ifndef __NRF24_HAL_H
#define __NRF24_HAL_H


#include "nrf24.h"


#define NRF24_HAL_YIELD_TIME_MS ((uint32_t)10)


void NRF24_HAL_FillCallbacks(NRF24_HalTypeDef *hHal);


#endif /* __NRF24_HAL_H */
