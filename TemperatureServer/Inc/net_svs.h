#ifndef __NET_SVS_H
#define __NET_SVS_H


#include "net.h"


typedef struct
{
    NET_HandleTypeDef NetHandle;
}
NET_SVS_HandleTypeDef;


void NET_SVS_StartTask(void const * argument);


#endif /* __NET_SVS_H */
