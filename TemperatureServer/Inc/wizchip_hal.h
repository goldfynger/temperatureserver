#ifndef __WIZCHIP_HAL_H
#define __WIZCHIP_HAL_H


#include "wizchip.h"


#define WIZ_HAL_YIELD_TIME_MS (10)


void WIZ_HAL_FillCallbacks(WIZ_HalTypeDef *hHal);


#endif /* __WIZCHIP_HAL_H */
