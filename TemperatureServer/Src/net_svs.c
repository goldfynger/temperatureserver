#include <string.h>

#include "dns.h"
#include "net.h"
#include "net_def.h"
#include "net_os.h"
#include "sntp.h"
#include "w5100.h"
#include "wizchip.h"
#include "wizchip_hal.h"

#include "TemperatureServer.config"

#include "net_svs.h"


extern void _Error_Handler(char *, int);

static void NET_SVS_Init(NET_SVS_HandleTypeDef *hNetSvs);


void NET_SVS_StartTask(void const * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_SVS_HandleTypeDef *hNetSvs = (NET_SVS_HandleTypeDef *)argument;
    
    NET_HandleTypeDef *hNet = &(hNetSvs->NetHandle);
    
    
    NET_SVS_Init(hNetSvs);
    
    
    DNS_HandleTypeDef *hDns = DNS_AllocateHandle(hNet, 8);
    
    
    SNTP_HandleTypeDef sntpHandle;
    memset(&sntpHandle, 0, sizeof(SNTP_HandleTypeDef));
    sntpHandle.NetHandle = hNet;
    sntpHandle.DnsHandle = hDns;
    
    SNTP_DateTimeTypeDef sntpDateTime;
    memset(&sntpDateTime, 0, sizeof(SNTP_DateTimeTypeDef));
    
    
    while (true)
    {
        if ((error = SNTP_DetermineDateTime(&(sntpHandle), SNTP_OFFSET_MSK, &sntpDateTime)) != WIZ_ERR_OK)
        {
            net_os_printf("Err %08X\r\n", error);
        
            _Error_Handler(__FILE__, __LINE__);
        }
        
        NET_OS_Delay(10000);
    }
}

static void NET_SVS_Init(NET_SVS_HandleTypeDef *hNetSvs)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_HandleTypeDef *hNet = &(hNetSvs->NetHandle);
    WIZ_HandleTypeDef *hWiz = &(hNet->WizHandle);
    
    
    WIZ_HAL_FillCallbacks(&(hWiz->Hal));
    
    W5100_GetChipApi(&(hWiz->ChipApi));
    
    if ((error = WIZ_Init(hWiz)) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    WIZ_MemorySizeTypeDef memSizes[W5100_SOCKET_COUNT] = { WIZ_MEMSIZE_2, WIZ_MEMSIZE_2, WIZ_MEMSIZE_2, WIZ_MEMSIZE_2 };
    
    if ((error = WIZ_SetMemorySizes(hWiz, memSizes, memSizes)) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    NET_Init(hNet);
    
    uint8_t macAddr[6] = { CONFIG_NET_SVS_MAC_ADDR_SRV_0, CONFIG_NET_SVS_MAC_ADDR_BYTE_4, CONFIG_NET_SVS_MAC_ADDR_BYTE_3, CONFIG_NET_SVS_MAC_ADDR_BYTE_2, CONFIG_NET_SVS_MAC_ADDR_BYTE_1, CONFIG_NET_SVS_MAC_ADDR_BYTE_0 };
    
    if ((error = NET_SetMacAddress(hNet, macAddr)) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NET_SetIPAddress(hNet, NET_CreateIPAddress(192, 168, 2, 240))) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NET_SetDefaultGateway(hNet, NET_CreateIPAddress(192, 168, 2, 1))) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NET_SetSubnetMask(hNet, NET_CreateIPAddress(255, 255, 255, 0))) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    NET_SetDNSServerAddress(hNet, NET_CreateIPAddress(192, 168, 2, 1));
    NET_SetSNTPServerName(hNet, "0.ru.pool.ntp.org");
}
