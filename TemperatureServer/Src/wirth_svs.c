#include <stdio.h>
#include <string.h>

#include "nrf24.h"
#include "nrf24_hal.h"

#include "TemperatureServer.config"

#include "wirth_svs.h"


extern void _Error_Handler(char *, int);

static void WIRTH_SVS_Init(WIRTH_SVS_HandleTypeDef *hWirthSvs);


void WIRTH_SVS_StartTask(void const * argument)
{
    WIRTH_SVS_HandleTypeDef *hWirth = (WIRTH_SVS_HandleTypeDef *)argument;
    
    
    WIRTH_SVS_Init(hWirth);
    
    
    // Task infinite loop.
    while (true)
    {
        memset(&(hWirth->Packet), 0, sizeof(WIRTH_SVS_PacketTypeDef));
        
        
        uint8_t receivedLength = 0;
        uint8_t receivedPipe = 0;
    
        if (NRF24_BeginReceive(&(hWirth->NrfHandle), (uint8_t *)(&(hWirth->Packet)), sizeof(WIRTH_SVS_PacketTypeDef), &receivedLength, &receivedPipe) != NRF24_ERR_OK)
        {
            _Error_Handler(__FILE__, __LINE__);
        }
        
        
        NRF24_ProcessTypeDef process = NRF24_PROCESS_NONE;
        
        if (NRF24_EndReceive(&(hWirth->NrfHandle), true, &process) != NRF24_ERR_OK)
        {
            _Error_Handler(__FILE__, __LINE__);
        }
        
        WIRTH_SVS_DeviceTypeDef *pDevice = &(hWirth->Devices[receivedPipe]);
        
        pDevice->UpTime = hWirth->Packet.UpTime;
        pDevice->ChipVoltage = hWirth->Packet.ChipVoltage;
        pDevice->ChipTemperature = hWirth->Packet.ChipTemperature;
        
        printf("%f %u %u\r\n", hWirth->Packet.MeterTemperature, receivedPipe, receivedLength);
        
        WIRTH_SVS_DataTypeDef data = { .Device = pDevice, .Temperature = hWirth->Packet.MeterTemperature, .Time = 0 };
        
        if (osMessagePut(hWirth->Queue, (uint32_t)&data, osWaitForever) != osOK)
        {
            _Error_Handler(__FILE__, __LINE__);
        }
    }
}

static void WIRTH_SVS_Init(WIRTH_SVS_HandleTypeDef *hWirthSvs)
{
    NRF24_HAL_FillCallbacks(&(hWirthSvs->NrfHandle.Hal));
    
    // Set mode.
    NRF24_SetMode(&(hWirthSvs->NrfHandle), (NRF24_ModeTypeDef)(NRF24_MODE_USE_RX |
        NRF24_MODE_USE_RX_P0 | NRF24_MODE_USE_RX_P1 | NRF24_MODE_USE_RX_P2 | NRF24_MODE_USE_RX_P3 | NRF24_MODE_USE_RX_P4 | NRF24_MODE_USE_RX_P5));
    
    // Set params.
    NRF24_SetFrequency(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_FREQUENCY);
    NRF24_SetRetrDelay(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_RETR_DELAY);
    NRF24_SetRetrCount(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_RETR_COUNT);
    NRF24_SetDataRate(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_DATA_RATE);
    NRF24_SetAmpPower(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_AMP_POWER);
    
    uint8_t rx0Address[NRF24_MAX_ADDR_LENGTH] = { CONFIG_WIRTH_SVS_ADDR_DEV_0, CONFIG_WIRTH_SVS_ADDR_BYTE_1, CONFIG_WIRTH_SVS_ADDR_BYTE_2, CONFIG_WIRTH_SVS_ADDR_BYTE_3, CONFIG_WIRTH_SVS_ADDR_BYTE_4 };
    uint8_t rx1Address[NRF24_MAX_ADDR_LENGTH] = { CONFIG_WIRTH_SVS_ADDR_DEV_1, CONFIG_WIRTH_SVS_ADDR_BYTE_1, CONFIG_WIRTH_SVS_ADDR_BYTE_2, CONFIG_WIRTH_SVS_ADDR_BYTE_3, CONFIG_WIRTH_SVS_ADDR_BYTE_4 };
    
    // Set adresses.
    NRF24_SetRx0Address(&(hWirthSvs->NrfHandle), rx0Address);
    NRF24_SetRx1Address(&(hWirthSvs->NrfHandle), rx1Address);
    NRF24_SetRx2Address(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_ADDR_DEV_2);
    NRF24_SetRx3Address(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_ADDR_DEV_3);
    NRF24_SetRx4Address(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_ADDR_DEV_4);
    NRF24_SetRx5Address(&(hWirthSvs->NrfHandle), CONFIG_WIRTH_SVS_ADDR_DEV_5);
    
    hWirthSvs->CommonAddressPart[0] = CONFIG_WIRTH_SVS_ADDR_BYTE_1;
    hWirthSvs->CommonAddressPart[1] = CONFIG_WIRTH_SVS_ADDR_BYTE_2;
    hWirthSvs->CommonAddressPart[2] = CONFIG_WIRTH_SVS_ADDR_BYTE_3;
    hWirthSvs->CommonAddressPart[3] = CONFIG_WIRTH_SVS_ADDR_BYTE_4;
    
    hWirthSvs->Devices[0].AddressLSByte = CONFIG_WIRTH_SVS_ADDR_DEV_0;
    hWirthSvs->Devices[1].AddressLSByte = CONFIG_WIRTH_SVS_ADDR_DEV_1;
    hWirthSvs->Devices[2].AddressLSByte = CONFIG_WIRTH_SVS_ADDR_DEV_2;
    hWirthSvs->Devices[3].AddressLSByte = CONFIG_WIRTH_SVS_ADDR_DEV_3;
    hWirthSvs->Devices[4].AddressLSByte = CONFIG_WIRTH_SVS_ADDR_DEV_4;
    hWirthSvs->Devices[5].AddressLSByte = CONFIG_WIRTH_SVS_ADDR_DEV_5;
    
    // Initialize nRF24.
    if (NRF24_Init(&(hWirthSvs->NrfHandle)) != NRF24_ERR_OK) _Error_Handler(__FILE__, __LINE__);
}
