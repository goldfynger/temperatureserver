#include "cmsis_os.h"
#include "main.h"
#include "net.h"
#include "spi.h"
#include "wizchip.h"

#include "wizchip_hal.h"


#define WIZ_HAL_SPI_TIMEOUT ((uint32_t)100)


extern osMutexId _SPI2_HAL_MutexHandle;


#define WIZ_HAL_SPI_HANDLE  (hspi2)
#define WIZ_HAL_SPI_MUTEX   (_SPI2_HAL_MutexHandle)


NET_ErrorTypeDef    WIZ_HAL_TransmitReceiveSync (uint8_t *pTxRxData, uint16_t length);
NET_ErrorTypeDef    WIZ_HAL_TakeMutex           (void);
NET_ErrorTypeDef    WIZ_HAL_ReleaseMutex        (void);
void                WIZ_HAL_ActivateNss         (void);
void                WIZ_HAL_DeactivateNss       (void);
void                WIZ_HAL_RunChip             (void);
void                WIZ_HAL_ResetChip           (void);
bool                WIZ_HAL_IsInt               (void);
void                WIZ_HAL_Yield               (void);


void WIZ_HAL_FillCallbacks(WIZ_HalTypeDef *hHal)
{
    hHal->WIZ_HAL_TransmitReceiveSync   = WIZ_HAL_TransmitReceiveSync;
    hHal->WIZ_HAL_TakeMutex             = WIZ_HAL_TakeMutex;
    hHal->WIZ_HAL_ReleaseMutex          = WIZ_HAL_ReleaseMutex;
    hHal->WIZ_HAL_ActivateNss           = WIZ_HAL_ActivateNss;
    hHal->WIZ_HAL_DeactivateNss         = WIZ_HAL_DeactivateNss;
    hHal->WIZ_HAL_RunChip               = WIZ_HAL_RunChip;
    hHal->WIZ_HAL_ResetChip             = WIZ_HAL_ResetChip;
    hHal->WIZ_HAL_IsInt                 = WIZ_HAL_IsInt;
    hHal->WIZ_HAL_Yield                 = WIZ_HAL_Yield;
}

NET_ErrorTypeDef WIZ_HAL_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length)
{
    return (HAL_SPI_TransmitReceive(&WIZ_HAL_SPI_HANDLE, pTxRxData, pTxRxData, length, WIZ_HAL_SPI_TIMEOUT) == HAL_OK) ? NET_ERR_OK : WIZ_ERR_HAL;
}

NET_ErrorTypeDef WIZ_HAL_TakeMutex(void)
{
    return (osRecursiveMutexWait(WIZ_HAL_SPI_MUTEX, osWaitForever) == osOK) ? NET_ERR_OK : WIZ_ERR_HAL;
}

NET_ErrorTypeDef WIZ_HAL_ReleaseMutex(void)
{
    return (osRecursiveMutexRelease(WIZ_HAL_SPI_MUTEX) == osOK) ? NET_ERR_OK : WIZ_ERR_HAL;
}

void WIZ_HAL_ActivateNss(void)
{
    HAL_GPIO_WritePin(WIZ_NSS_GPIO_Port, WIZ_NSS_Pin, GPIO_PIN_RESET);
}

void WIZ_HAL_DeactivateNss(void)
{
    HAL_GPIO_WritePin(WIZ_NSS_GPIO_Port, WIZ_NSS_Pin, GPIO_PIN_SET);
}

void WIZ_HAL_RunChip(void)
{
    HAL_GPIO_WritePin(WIZ_RST_GPIO_Port, WIZ_RST_Pin, GPIO_PIN_SET);
}

void WIZ_HAL_ResetChip(void)
{
    HAL_GPIO_WritePin(WIZ_RST_GPIO_Port, WIZ_RST_Pin, GPIO_PIN_RESET);
}

bool WIZ_HAL_IsInt(void)
{
    return (HAL_GPIO_ReadPin(WIZ_INT_GPIO_Port, WIZ_INT_Pin) == GPIO_PIN_RESET);
}

void WIZ_HAL_Yield(void)
{
    //osThreadYield(); -> Trace: SW Buffer Overrun.
    osDelay(WIZ_HAL_YIELD_TIME_MS);
}
