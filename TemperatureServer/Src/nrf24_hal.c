#include "cmsis_os.h"
#include "main.h"
#include "nrf24.h"
#include "spi.h"

#include "nrf24_hal.h"


#define NRF24_HAL_SPI_TIMEOUT   ((uint32_t)100)


extern osMutexId _SPI1_HAL_MutexHandle;

uint8_t _NRF24_HAL_RxTmpBuffer[NRF24_MAX_DATA_LENGTH];


#define NRF24_HAL_SPI_HANDLE    (hspi1)
#define NRF24_HAL_SPI_MUTEX     (_SPI1_HAL_MutexHandle)


NRF24_ErrorTypeDef NRF24_HAL_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length);
NRF24_ErrorTypeDef NRF24_HAL_TransmitSync(uint8_t *pTxData, uint16_t length);
NRF24_ErrorTypeDef NRF24_HAL_ReceiveSync(uint8_t *pRxData, uint16_t length);
NRF24_ErrorTypeDef NRF24_HAL_TakeControl(void);
NRF24_ErrorTypeDef NRF24_HAL_ReleaseControl(void);
void NRF24_HAL_ActivateCe(void);
void NRF24_HAL_DeactivateCe(void);
bool NRF24_HAL_IsIrq(void);
void NRF24_HAL_Yield(void);
static void NRF24_HAL_ActivateNss(void);
static void NRF24_HAL_DeactivateNss(void);
static NRF24_ErrorTypeDef NRF24_HAL_TakeMutex(void);
static NRF24_ErrorTypeDef NRF24_HAL_ReleaseMutex(void);


void NRF24_HAL_FillCallbacks(NRF24_HalTypeDef *hHal)
{
    hHal->NRF24_HAL_TransmitReceiveSync = NRF24_HAL_TransmitReceiveSync;
    hHal->NRF24_HAL_TransmitSync = NRF24_HAL_TransmitSync;
    hHal->NRF24_HAL_ReceiveSync = NRF24_HAL_ReceiveSync;
    hHal->NRF24_HAL_TakeControl = NRF24_HAL_TakeControl;
    hHal->NRF24_HAL_ReleaseControl = NRF24_HAL_ReleaseControl;    
    hHal->NRF24_HAL_ActivateCe = NRF24_HAL_ActivateCe;
    hHal->NRF24_HAL_DeactivateCe = NRF24_HAL_DeactivateCe;
    hHal->NRF24_HAL_IsIrq = NRF24_HAL_IsIrq;
    hHal->NRF24_HAL_Yield = NRF24_HAL_Yield;
}

NRF24_ErrorTypeDef NRF24_HAL_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length)
{
    return (HAL_SPI_TransmitReceive(&NRF24_HAL_SPI_HANDLE, pTxRxData, pTxRxData, length, NRF24_HAL_SPI_TIMEOUT) != HAL_OK) ? NRF24_ERR_HAL : NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_HAL_TransmitSync(uint8_t *pTxData, uint16_t length)
{
    if (length > NRF24_MAX_DATA_LENGTH) return NRF24_ERR_HAL;
    
    return (HAL_SPI_TransmitReceive(&NRF24_HAL_SPI_HANDLE, pTxData, _NRF24_HAL_RxTmpBuffer, length, NRF24_HAL_SPI_TIMEOUT) != HAL_OK) ? NRF24_ERR_HAL : NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_HAL_ReceiveSync(uint8_t *pRxData, uint16_t length)
{
    return (HAL_SPI_TransmitReceive(&NRF24_HAL_SPI_HANDLE, pRxData, pRxData, length, NRF24_HAL_SPI_TIMEOUT) != HAL_OK) ? NRF24_ERR_HAL : NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_HAL_TakeControl(void)
{
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    
    if ((error = NRF24_HAL_TakeMutex()) != NRF24_ERR_OK) return error;
    
    NRF24_HAL_ActivateNss();
    
    return error;
}

NRF24_ErrorTypeDef NRF24_HAL_ReleaseControl(void)
{
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    
    if ((error = NRF24_HAL_ReleaseMutex()) != NRF24_ERR_OK) return error;
    
    NRF24_HAL_DeactivateNss();
    
    return error;
}

void NRF24_HAL_ActivateCe(void)
{
    HAL_GPIO_WritePin(NRF_CE_GPIO_Port, NRF_CE_Pin, GPIO_PIN_SET);
}

void NRF24_HAL_DeactivateCe(void)
{
    HAL_GPIO_WritePin(NRF_CE_GPIO_Port, NRF_CE_Pin, GPIO_PIN_RESET);
}

bool NRF24_HAL_IsIrq(void)
{
    return (HAL_GPIO_ReadPin(NRF_IRQ_GPIO_Port, NRF_IRQ_Pin) == GPIO_PIN_RESET);
}

void NRF24_HAL_Yield(void)
{
    //osThreadYield(); -> Trace: SW Buffer Overrun.
    osDelay(NRF24_HAL_YIELD_TIME_MS);
}

static void NRF24_HAL_ActivateNss(void)
{
    HAL_GPIO_WritePin(NRF_NSS_GPIO_Port, NRF_NSS_Pin, GPIO_PIN_RESET);
}

static void NRF24_HAL_DeactivateNss(void)
{
    HAL_GPIO_WritePin(NRF_NSS_GPIO_Port, NRF_NSS_Pin, GPIO_PIN_SET);
}

static NRF24_ErrorTypeDef NRF24_HAL_TakeMutex(void)
{
    return (osRecursiveMutexWait(NRF24_HAL_SPI_MUTEX, osWaitForever) == osOK) ? NRF24_ERR_OK : NRF24_ERR_HAL;
}

static NRF24_ErrorTypeDef NRF24_HAL_ReleaseMutex(void)
{
    return (osRecursiveMutexRelease(NRF24_HAL_SPI_MUTEX) == osOK) ? NRF24_ERR_OK : NRF24_ERR_HAL;
}
